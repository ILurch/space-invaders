﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Lifetime;

namespace GXPEngine
{
    public class BulletType
    {
        public int ID => _id;
        public int Speed => _speed;
        public int Damage => _damage;
        public string Sound => _sound;

        private int _id;
        private int _speed;
        private int _damage;
        private string _sound;
        
        public BulletType(int id, int speed, int damage, string sound)
        {
            _id = id;
            _speed = speed;
            _damage = damage;
            _sound = sound;
        }
    }

    public class Bullet : AnimationSprite
    {
        public static BulletType PLAYER = new BulletType(0, 26, 4, "Laser-2.wav");
        public static BulletType ROCKET = new BulletType(2, 15, 6, "Laser.wav");
        public static BulletType ENEMY = new BulletType(3, 8, 10, "Enemy Laser.wav");

        public BulletType Type => _type;
        public Vec2 Position => _position;

        private Vec2 _position = Vec2.GetZero();
        private BulletType _type;

        public Bullet(BulletType type, float rotation) : base("assets/Bullet_Sheet.png", 1, 4)
        {
            SetScaleXY(0.7f, 0.7f);
            SetOrigin(width / 2f, height / 1.5f);
            _type = type;
            SetFrame(_type.ID);
            this.rotation = rotation;
            updatePosition();
        }

        public override void Destroy()
        {
            
            SpaceInvaders.instance.Level.Bullets.Remove(this);
            base.Destroy();
            
        }

        public void Update()
        {
            if (SpaceInvaders.instance.Level.Paused) return;

            Vec2 velo;
            if (_type.Equals(PLAYER))
            {
                velo = new Vec2(_position.Clone() - SpaceInvaders.instance.Level.Planet.Position).Normalize()
                    .Scale(_type.Speed);
            }
            else if (_type.Equals(ENEMY))
            {
                velo = new Vec2(SpaceInvaders.instance.Level.Planet.Position.Clone() - _position).Normalize()
                    .Scale(_type.Speed);
            }
            else
            {
                return;
            }

            _rotation = velo.GetAngle(true);
            _position.Add(velo);
            //Destroy bullet if out of vision
            float radius = SpaceInvaders.instance.Level.Planet.Position.Clone().Subtract(Position).Length();
            if (radius > 3000)
                Destroy();

            updatePosition();
        }

        public bool CheckColision(Sprite obj)
        {
            Vec2 pos = null;
            if (obj is Player)
            {
                pos = ((Player) obj).Position;
            }
            else if (obj is Enemy)
            {
                pos = ((Enemy) obj).Position;
            }

            if (pos == null) return false;

            float leftDist = _position.DistanceSquared(pos.Clone().Add(0, height / 4f));
            float rightDist = _position.DistanceSquared(pos.Clone().Subtract(0, height / 4f));

            //Console.WriteLine("Left: " + leftDist + " Right: " + rightDist);
            return (leftDist < (obj.height / 2.5f) || rightDist < (obj.height / 2.5f));
        }

        private void updatePosition()
        {
            SetXY(_position.x, _position.y);
        }
    }
}