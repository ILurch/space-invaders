﻿namespace GXPEngine
{
    public class Collectable : Sprite
    {
        public class ColType
        {
            public string File => _file;

            private string _file;

            public ColType(string file)
            {
                _file = file;
            }
        }

        public static readonly ColType STAR = new ColType("assets/powerup/star.tif");
        public static readonly ColType ENEMY_SLOW = new ColType("assets/powerup/enemy_slow.png");

        public ColType Type => _colType;
        public Vec2 Position => _position;

        private Vec2 _position;
        private ColType _colType;

        private float maxScale = 1.3f;
        private float minScale = 0.9f;
        private const float step = 0.01f;
        private bool up = true;

        public Collectable(Vec2 position, ColType colType) : base(colType.File)
        {
            SetOrigin(width / 2f, height / 2f);
            _colType = colType;
            _position = position;
        }

        public void Update()
        {
            if (scale >= maxScale)
            {
                SetScaleXY(maxScale);
                up = false;
            }

            if (scale <= minScale)
            {
                SetScaleXY(minScale);
                up = true;
            }

            float currStep = step;
            if (!up) currStep *= -1;

            SetScaleXY(scale + currStep);

            SetXY(_position.x, _position.y);
        }

        public override void Destroy()
        {
            SpaceInvaders.instance.Level.Collectables.Remove(this);
            base.Destroy();
        }
    }
}