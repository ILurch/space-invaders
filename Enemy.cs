﻿using System;

namespace GXPEngine
{
    public class EnemyType
    {
        public string Sprite => _sprite;
        public int Health => _health;
        public bool HasGun => _hasGun;

        private string _sprite;
        private int _health;
        private bool _hasGun;

        public EnemyType(string sprite, int health, bool hasGun)
        {
            _sprite = sprite;
            _health = health;
            _hasGun = hasGun;
        }
    }

    public class Enemy : Sprite, Vulnerable
    {
        public static readonly EnemyType DEFAULT = new EnemyType("assets/character/enemies 1.png", 30, false);
        public static readonly EnemyType SHOOTER = new EnemyType("assets/character/enemies 2.png", 8, true);
        public static readonly EnemyType KAMIKAZE = new EnemyType("assets/character/enemies 3.png", 4, false);

        public Vec2 Position => _position;
        public EnemyType Type => _type;

        private Vec2 _position;
        private Vec2 _velocity = Vec2.GetZero();
        private EnemyType _type;
        private int _currentHealth;

        private const float speed = 0.6f;

        private Weapon _defaultGun;

        private int count = 0;
        private int delay;
        private int minDelay = 150;
        private int maxDelay = 200;

        private Healthbar _healhbar;

        public Enemy(EnemyType type, Vec2 pos) : base(type.Sprite)
        {
            SetOrigin(width / 2f, height / 2f);
            _type = type;
            _currentHealth = type.Health;
            _position = pos;
            updatePosition();

            if (_type.HasGun)
            {
                _defaultGun = new Weapon(this, Bullet.ENEMY, 10);
                _defaultGun.Clip = Int32.MaxValue;
                delay = Utils.Random(minDelay, maxDelay);
            }

            _healhbar = new Healthbar(this);
            _healhbar.visible = false;
            _healhbar.SetXY(-height / 2.7f, 0);
            _healhbar.rotation = 270;
            // AddChild(_healhbar);
        }

        public override void Destroy()
        {
            SpaceInvaders.instance.Level.Enemies.Remove(this);
            base.Destroy();
        }

        public void Update()
        {
            if (SpaceInvaders.instance.Level == null || SpaceInvaders.instance.Level.Paused) return;

            //Console.WriteLine(_currentHealth);
            if (_currentHealth <= 0)
            {
                Destroy();
                return;
            }

            _healhbar.Update(_type.Health, _currentHealth);

            _velocity = new Vec2(SpaceInvaders.instance.Level.Planet.Position.Clone() - _position).Normalize()
                .Scale(speed);
            rotation = _velocity.GetAngle(true);
            _position.Add(_velocity);
            updatePosition();

            if (_type.HasGun)
                calcShooting();
        }

        private void calcShooting()
        {
            if (count > delay)
            {
                _defaultGun.Shoot();
                count = 0;
                delay = Utils.Random(minDelay, maxDelay);
            }

            count++;
        }

        private void updatePosition()
        {
            SetXY(_position.x, _position.y);
        }

        public void recieveDamge(int damage)
        {
            _healhbar.visible = true;
            _currentHealth -= damage;
            // Console.WriteLine(damage);
            Sound sound = new Sound("assets/audio/Hit.wav");
            sound.Play();
        }

        public bool Alive()
        {
            return _currentHealth > 0;
        }
    }
}