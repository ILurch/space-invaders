﻿namespace GXPEngine
{
    public class GameOver : Sprite
    {
        public GameOver() : base("assets/overlay/gameover.png")
        {
            SetOrigin(width / 2f, height / 2f);
        }

        public void Show()
        {
            visible = true;
            if (parent != null)
                parent.SetChildIndex(this, 10000000);
        }

        public void Hide()
        {
            visible = false;
        }
    }
}