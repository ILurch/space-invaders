﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using System.Runtime;

namespace GXPEngine
{
    public class Background : Canvas
    {
        private Bitmap image = (Bitmap) Image.FromFile("assets/background.png");

        private static Vec2[] _spritePos = new[]
        {
            new Vec2(0, -1), new Vec2(0, 0), new Vec2(0, 1), new Vec2(0, 2),
            new Vec2(1, -1), new Vec2(1, 0), new Vec2(1, 1), new Vec2(1, 2),
            new Vec2(-1, -1), new Vec2(-1, 0), new Vec2(-1, 1), new Vec2(-1, 2)
        };

        List<Sprite> _sprites = new List<Sprite>();

        public Background() : base(1920 * 2, 1080)
        {
            foreach (Vec2 v in _spritePos)
            {
                Sprite spr = new Sprite(image);
                spr.SetXY(1920 * v.x, 1080 * v.y);
                AddChild(spr);
                _sprites.Add(spr);
            }

            Sprite planet1 = new Sprite("assets/planet 1.png");
            planet1.SetOrigin(-150, -40);
            AddChild(planet1);
            _sprites.Add(planet1);

            Sprite planet2 = new Sprite("assets/planet 2.png");
            //planet2.SetScaleXY(0.5f, 0.5f);
            planet2.SetOrigin(-1300, -2340);
            AddChild(planet2);
            _sprites.Add(planet2);
            
            //Vec2 origin = SpaceInvaders.instance.Level.Planet.Position.Clone();
            //SetOrigin(origin.x, origin.y);
        }

        public void Rotate(float angle)
        {
            foreach (Sprite spr in _sprites)
            {
                rotate(angle, spr);
            }
        }

        private void rotate(float angle, Sprite spr)
        {
            Vec2 center = SpaceInvaders.instance.Level.Planet.Position.Clone();
            Vec2 topPos = new Vec2(spr.x, spr.y);
            topPos.RotateAround(center, angle, true);
            spr.SetXY(topPos.x, topPos.y);
            spr.rotation += angle;
        }
    }
}