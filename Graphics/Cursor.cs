﻿using System;

namespace GXPEngine
{
    public class Cursor : Sprite
    {
        private Vec2 _position;

        public Cursor(string file) : base(file)
        {
            _position = new Vec2(0, 0);
        }

        void Update()
        {
            _position.SetXY(Input.mouseX, Input.mouseY);

            x = _position.x;
            y = _position.y;
        }
    }
}