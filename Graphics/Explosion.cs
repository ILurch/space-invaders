﻿namespace GXPEngine
{
    public class Explosion : AnimationSprite
    {

        public Vec2 Position => _position;
        
        private Vec2 _position;
        private Animation _animation;

        public Explosion(Vec2 position) : base("assets/exploision animation.png", 5, 1)
        {
            SpaceInvaders.instance.Level.Explosions.Add(this);
            _position = position;
            SetOrigin(width / 2f, height / 2f);
            SetScaleXY(3, 3);
            int[] order = new int[] {0, 1, 2, 3, 4};
            _animation = new Animation(order, 5);
            Sound exploSound = new Sound("assets/audio/Explode.wav");
            exploSound.Play();
        }
        
        public void Update()
        {
            SetXY(_position.x, _position.y);
            SetFrame(_animation.Count);
            if (_animation.Count == 4)
            {
                Destroy();
            }
            _animation.Update();
        }
    }
}