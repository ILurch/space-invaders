﻿using System.Drawing;

namespace GXPEngine
{
    public class FPS : Canvas
    {
        private int frames;
        private float deltaTime;

        public FPS() : base(1920, 1080)
        {
        }

        public void Update()
        {
            graphics.Clear(Color.Transparent);
            graphics.DrawString("" + SpaceInvaders.FPS, new Font("Arial.tff", 13), new SolidBrush(Color.LightSeaGreen),
                0, 0);
            calcFPS();
        }

        private void calcFPS()
        {
            frames++;
            deltaTime += Time.deltaTime;
            if (deltaTime >= 1000)
            {
                SpaceInvaders.FPS = frames;
                frames = 0;
                deltaTime = 0;
            }
        }
    }
}