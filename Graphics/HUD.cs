﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GXPEngine
{
    public class HUD : Canvas
    {
        public GraphicNumber ScoreNumber => _scoreNumber;
        public GraphicNumber WaveNumber => _waveNumber;

        private GraphicNumber _waveNumber;
        private GraphicNumber _scoreNumber;
        private AnimationSprite _planetHealthbar;

        private Sprite _wave;
        private Sprite _score;

        public HUD(int width, int height) : base(width, height)
        {
            //SetOrigin(width / 2f, height / 2f);
            _wave = new Sprite("assets/overlay/wave.png");
            _score = new Sprite("assets/overlay/score.png");
            _planetHealthbar = new AnimationSprite("assets/overlay/Health.png", 1, 8);

            _wave.SetOrigin(_wave.width / 2f, _wave.height / 2f);
            _score.SetOrigin(_score.width / 2f, _score.height / 2f);
            _planetHealthbar.SetOrigin(_planetHealthbar.width / 2f, _planetHealthbar.height / 2f);

            int screenX = Settings.GetInt("ScreenResolutionX");
            int screenY = Settings.GetInt("ScreenResolutionY");

            float heightF = Settings.GetFloat("HUDHeightFactor");
            _score.SetXY(-screenX / 3f, screenY / heightF);
            _wave.SetXY(screenX / 3f, screenY / heightF);
            _planetHealthbar.SetXY(0, screenY / heightF);


            //  Console.WriteLine(screenX + " " + screenY);
            _waveNumber = new GraphicNumber(this, new Vec2(_wave.x, _wave.y - 50), margin: 0);
            _scoreNumber = new GraphicNumber(this, new Vec2(_score.x, _score.y - 50), margin: 0);

            SetWaveCount(0);
            SetScore(0);
            AddChild(_wave);
            AddChild(_score);
            AddChild(_planetHealthbar);
        }

        public void OnGameOver()
        {
            int screenX = Settings.GetInt("ScreenResolutionX");
            int screenY = Settings.GetInt("ScreenResolutionY");

            _score.SetXY(-screenX / 6f, screenY / 4f);
            _wave.SetXY(screenX / 6f, screenY / 4f);
            
            _waveNumber.Position.SetXY(_wave.x, _wave.y - 50);
            _scoreNumber.Position.SetXY(_score.x, _score.y - 50);
            _scoreNumber.Update();
            _waveNumber.Update();

            _planetHealthbar.Destroy();
        }

        public void SetWaveCount(int waveCount)
        {
            _waveNumber.Update(waveCount);
        }

        public void SetPlanetHealth(int current, int max)
        {
            float percentage = current / (float) max;
            int frame = (int) (_planetHealthbar.frameCount * (1 - percentage));
            _planetHealthbar.SetFrame(frame);
        }

        public void SetScore(int score)
        {
            _scoreNumber.Update(score);
        }
    }
}