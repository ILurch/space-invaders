﻿namespace GXPEngine
{
    public class Shadow : Sprite
    {
        private Sprite _spriteToAttachTo;
        private Vec2 _offset;

        public Shadow(Sprite spriteToAttachTo, Vec2 offset)
            : base(spriteToAttachTo.name)
        {
            _spriteToAttachTo = spriteToAttachTo;
            _offset = offset;

            SetOrigin(width / 2f, height / 2f);
            alpha = 0.32f;
            color = 0x00000000;
        }

        private void adjustPosition()
        {
            if (null != _spriteToAttachTo)
            {
                x = _spriteToAttachTo.x + _offset.x;
                y = _spriteToAttachTo.y + _offset.y;
                rotation = _spriteToAttachTo.rotation;
            }
        }
    }
}