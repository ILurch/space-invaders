﻿using System;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization.Formatters;

namespace GXPEngine
{
    public class Healthbar : Canvas
    {
        public enum Type
        {
            HALF_CIRCLE,
            BAR,
        }
        
        private bool destroied;

        public Healthbar(Sprite spr) : base(100, 8)
        {
            SetOrigin(width / 2f, height / 2f);
            spr.AddChild(this);
        }

        public void Update(int maxHealth, int currentHealth)
        {
            float percent = currentHealth / (float) maxHealth;
            draw(percent);
        }

        public override void Destroy()
        {
            destroied = true;
            base.Destroy();
        }

        public void FadeOut(int updates)
        {
        }

        private void draw(float percent)
        {
            if (destroied) return;
            graphics.Clear(Color.Transparent);
            graphics.FillRectangle(new SolidBrush(Color.LimeGreen), 0, 0, width * percent, height);
            graphics.FillRectangle(new SolidBrush(Color.Red), width * percent, 0, width - (width * percent), height);
        }
    }
}