﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Services;

namespace GXPEngine
{
    public class Level : GameObject
    {
        public bool Paused { get; set; }

        public Player Player => _player;
        public Planet Planet => _planet;
        public List<Enemy> Enemies => _enemies;
        public List<Bullet> Bullets => _bullets;
        public List<Explosion> Explosions => _explosions;
        public List<Collectable> Collectables => _collectables;
        public float DamageMultiplier => _damageMultiplier;

        private Planet _planet;
        private Player _player;

        private float _damageMultiplier = 1.1f;

        private WaveManager _waveManager;
        private List<Enemy> _enemies = new List<Enemy>();
        private List<Bullet> _bullets = new List<Bullet>();
        private List<Explosion> _explosions = new List<Explosion>();
        private List<Collectable> _collectables = new List<Collectable>();

        private GameOver _gameOver;

        private Background _background;

        private int maxCollectables = 3;
        private const int minColSpawn = 500;
        private const int maxColSpawn = 2500;

        private int colCount;
        private int nextColSpawn = 0;

        private new SpaceInvaders game;

        public Level(SpaceInvaders game)
        {
            this.game = game;
            _planet = new Planet();
            _player = new Player();
            _waveManager = new WaveManager();

            _planet.Position.SetXY(game.width / 2f, game.height + (_planet.height / 4));
            _player.Position.SetXY(game.width / 2f, game.height / 2f);

            _gameOver = new GameOver();
            _background = new Background();

            AddChild(_background);
            AddChild(_player);
            AddChild(_planet);
            AddChild(_gameOver);

            _gameOver.Hide();
            _gameOver.SetXY(game.width / 2f, game.height / 2f);
        }


        public void Update()
        {
            if (!Paused)
                _waveManager.Update();
            checkColisions();
            collectableSpawnCounter();
            if (_player.Health <= 0 || _planet.Health <= 0)
            {
                onGameOver();
                game.HUD.OnGameOver();
            }
        }

        private void collectableSpawnCounter()
        {
            if (_collectables.Count < maxCollectables)
            {
                if (colCount == 0)
                {
                    nextColSpawn = Utils.Random(minColSpawn, maxColSpawn);
                }

                if (colCount == nextColSpawn)
                {
                    int type = Utils.Random(0, 2);
                    if (type == 0)
                    {
                        spawnCollectable(Collectable.ENEMY_SLOW);
                    }
                    else
                    {
                        spawnCollectable(Collectable.STAR);
                    }

                    colCount = 0;
                }
                else
                {
                    colCount++;
                }
            }
        }

        private void checkColisions()
        {
            List<GameObject> colliders = new List<GameObject>();

            colliders.AddRange(_enemies);
            colliders.Add(_player);

            foreach (Bullet bullet in _bullets.ToArray())
            {
                foreach (GameObject col in colliders)
                {
                    Sprite obj = (Sprite) col;
                    if (bullet.Type.Equals(Bullet.PLAYER) && obj is Player) continue;
                    if (bullet.Type.Equals(Bullet.ROCKET) && obj is Player) continue;
                    if (bullet.Type.Equals(Bullet.ENEMY) && obj is Enemy) continue;

                    if (bullet.CheckColision(obj))
                    {
                        (obj as Vulnerable)?.recieveDamge((int) (bullet.Type.Damage * DamageMultiplier));
                        bullet.Destroy();
                        if (obj is Enemy && !(obj as Vulnerable).Alive())
                        {
                            Enemy enemy = (Enemy) obj;
                            if (enemy.Type.Equals(Enemy.DEFAULT))
                            {
                                Player.AddScore(20);
                            }
                            else if (enemy.Type.Equals(Enemy.KAMIKAZE))
                            {
                                Player.AddScore(10);
                            }
                            else if (enemy.Type.Equals(Enemy.SHOOTER))
                            {
                                Player.AddScore(15);
                            }

                            SpaceInvaders.instance.HUD.SetScore(Player.Score);
                        }

                        Explosion ex = new Explosion(bullet.Position);
                        AddChild(ex);
                        _explosions.Add(ex);
                    }
                }

                float radius = Planet.Position.Clone().Subtract(bullet.Position).Length();
                if (radius > 3000)
                    bullet.Destroy();
                else if (radius < 500)
                {
                    _planet.recieveDamge((int) (bullet.Type.Damage / 2f));
                    bullet.Destroy();
                    Explosion ex = new Explosion(bullet.Position);
                    AddChild(ex);
                    _explosions.Add(ex);
                    //Console.WriteLine(_planet.Health);
                }
            }

            foreach (Enemy enemy in _enemies.ToArray())
            {
                if (enemy.Position.DistanceSquared(Player.Position) < 100)
                {
                    enemy.Destroy();
                    _enemies.Remove(enemy);
                    new Sound("assets/audio/hit2.wav").Play();
                    Explosion ex = new Explosion(enemy.Position);
                    _explosions.Add(ex);
                    AddChild(ex);
                    _player.recieveDamge(30);
                }

                float radius = Planet.Position.Clone().Subtract(enemy.Position).Length();
                if (radius < 500)
                {
                    enemy.Destroy();
                    _enemies.Remove(enemy);
                    new Sound("assets/audio/hit2.wav").Play();
                    Explosion ex = new Explosion(enemy.Position);
                    _explosions.Add(ex);
                    AddChild(ex);
                    _planet.recieveDamge(30);
                }
            }

            foreach (Collectable col in _collectables.ToArray())
            {
                float leftDist = col.Position.DistanceSquared(Player.Position.Clone().Add(0, Player.height / 4f));
                float rightDist = col.Position.DistanceSquared(Player.Position.Clone().Subtract(0, Player.height / 4f));

                //Console.WriteLine("Left: " + leftDist + " Right: " + rightDist);

                if (leftDist < (Player.height / 2.2f) || rightDist < (Player.height / 2.2f))
                {
                    if (col.Type.Equals(Collectable.ENEMY_SLOW))
                    {
                        _player.Health += 15;
                    }
                    else if (col.Type.Equals(Collectable.STAR))
                    {
                        Player.AddScore(15);
                        SpaceInvaders.instance.HUD.SetScore(Player.Score);
                        _planet.Health += 25;
                    }

                    col.Destroy();
                    Sound sound = new Sound("assets/audio/Select.wav");
                    sound.Play();
                }
            }
        }

        public void onGameOver()
        {
            _gameOver.Show();
            if (Player.Health <= 0)
                Player.Destroy();
            Paused = true;
        }

        public void RotateLevel(float angle)
        {
            _planet.rotation -= angle;
            _background.Rotate(-angle);
            foreach (Enemy enemy in _enemies)
            {
                enemy.Position.RotateAround(_planet.Position, -angle, true);
            }

            foreach (Bullet bullet in _bullets)
            {
                bullet.Position.RotateAround(_planet.Position, -angle, true);
                bullet.rotation -= angle;
            }

            foreach (Explosion ex in _explosions)
            {
                ex.rotation -= angle / 2;
                ex.Position.RotateAround(_planet.Position, -angle / 2, true);
            }

            foreach (Collectable col in _collectables)
            {
                col.rotation -= angle;
                col.Position.RotateAround(_planet.Position, -angle, true);
            }
        }

        private void spawnCollectable(Collectable.ColType type)
        {
            Vec2 v = new Vec2(Utils.Random(0, 360), true);
            v.Scale(Utils.Random(Player.MinDist + 5, Player.MaxDist + 5));
            Collectable col = new Collectable(_planet.Position.Clone().Add(v), type);
            AddChild(col);
            _collectables.Add(col);
        }

        public bool IsOver()
        {
            return _gameOver.visible;
        }
    }
}