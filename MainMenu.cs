﻿using System;
using System.Runtime.InteropServices;

namespace GXPEngine
{
    public class MainMenu : Canvas
    {
        public Sprite Overlay => overlay;
        
        private Sprite overlay;
        private Sprite htp;

        private int _selection;

        private AnimationSprite[] buttons;

        public MainMenu(int width, int height) : base(width, height)
        {
            SetOrigin(width / 2f, height / 2f);

            overlay = new Sprite("assets/overlay/main.png");
            overlay.SetOrigin(overlay.width / 2f, overlay.height / 2f);

            htp = new Sprite("assets/overlay/selection/htp.png");
            htp.SetOrigin(htp.width / 2f, htp.height / 2f);
            htp.visible = false;

            Sprite bg = new Sprite("assets/overlay/selection/Background.png");
            bg.SetOrigin(bg.width / 2f, bg.height / 2f);

            AddChild(htp);
            AddChild(bg);
            initButtons();
            AddChild(overlay);
        }

        private void initButtons()
        {
            AnimationSprite start = new AnimationSprite("assets/overlay/selection/buttons/game.png", 1, 2);
            AnimationSprite htp = new AnimationSprite("assets/overlay/selection/buttons/htp.png", 1, 2);
            AnimationSprite quit = new AnimationSprite("assets/overlay/selection/buttons/quit.png", 1, 2);

            buttons = new[] {start, htp, quit};

            start.SetOrigin(start.width / 2f, start.height / 2f);
            htp.SetOrigin(htp.width / 2f, htp.height / 2f);
            quit.SetOrigin(quit.width / 2f, quit.height / 2f);


            start.SetXY(0, 150);
            htp.SetXY(0, start.y + start.height + 25);
            quit.SetXY(0, htp.y + htp.height + 25);

            AddChild(start);
            AddChild(htp);
            AddChild(quit);
        }

        public void Update()
        {
            if (overlay.visible)
            {
                if (Input.GetKeyDown(Settings.Menu))
                {
                    overlay.visible = false;
                }
            }
            else
            {
                if (!htp.visible)
                {
                    if (Input.GetKeyDown(Settings.Menu))
                    {
                        overlay.visible = true;
                    }

                    controlSelection();
                    if (Input.GetKeyDown(Settings.P1Fire1))
                    {
                        switch (_selection)
                        {
                            case(0):
                                SpaceInvaders.instance.StartGame();
                                break;
                            case(1):
                                SetChildIndex(htp, 100000);
                                htp.visible = true;
                                break;
                            case(2):
                                SpaceInvaders.instance.Destroy();
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    if (Input.GetKeyDown(Settings.Menu))
                    {
                        htp.visible = false;
                    }
                }
            }
        }

        private void controlSelection()
        {
            if (Input.GetKeyDown(Settings.P1Down))
            {
                _selection++;
                new Sound("assets/audio/Select.wav").Play();
            }
            else if (Input.GetKeyDown(Settings.P1Up))
            {
                _selection--;
                new Sound("assets/audio/Select.wav").Play();
            }

            if (_selection > 2) _selection = 0;
            if (_selection < 0) _selection = 2;

            for (int i = 0; i < buttons.Length; i++)
            {
                if (i == _selection)
                {
                    buttons[i].SetFrame(1);
                }
                else
                {
                    buttons[i].SetFrame(0);
                }
            }
        }
    }
}