﻿using System.Xml.Serialization;

namespace GXPEngine.Parse
{
    [XmlRoot("wave")]
    public class Wave
    {
        [XmlElement("enemy")]
        public WaveEnemy[] Enemies;
    }
}