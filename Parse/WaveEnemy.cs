﻿using System.Xml.Serialization;

namespace GXPEngine.Parse
{
    [XmlRoot("enemy")]
    public class WaveEnemy
    {
        [XmlAttribute("rotation")]
        public int rotation;

        [XmlAttribute("delay")]
        public int delay;

        [XmlAttribute("type")]
        public string type;
    }
}