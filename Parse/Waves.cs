﻿using System.Xml.Serialization;

namespace GXPEngine.Parse
{
    [XmlRoot("waves")]
    public class Waves
    {
        [XmlAttribute("delay")] 
        public int delay;
        
        [XmlElement("wave")]
        public Wave[] waves;
    }
}