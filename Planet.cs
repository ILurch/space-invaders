﻿using System;

namespace GXPEngine
{
    public class Planet : AnimationSprite, Vulnerable
    {
        public int radius => width / 2;
        public Vec2 Position => _position;
        public int Health
        {
            get => health;
            set => health = value;
        }

        private int maxHealth = 250;
        
        private int health;
        private readonly Vec2 _position;
        
        public Planet() : base("assets/earth.png", 1, 1)
        {
            health = maxHealth;
            SetOrigin(width / 2f, height / 2f);
            _position = Vec2.GetZero();
        }

        public void Update()
        {
            if (health <= 0)
            {
                SpaceInvaders.instance.Level.onGameOver();
            }
            updatePosition();
        }
        
        private void updatePosition()
        {
            SetXY(_position.x, _position.y);
        }

        public void recieveDamge(int damage)
        {
            health -= damage;
            SpaceInvaders.instance.HUD.SetPlanetHealth(health, maxHealth);
        }

        public bool Alive()
        {
           return health > 0;
        }
    }
}