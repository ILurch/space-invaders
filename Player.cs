﻿using System;
using System.Linq.Expressions;
using System.Security.AccessControl;

namespace GXPEngine
{
    public class Player : AnimationSprite, Vulnerable
    {
        public Vec2 Position => _position;
        public float RotateVelo => _rotateVelo;
        public float Alpha => _alpha;
        public Vec2 Velocity => _velocity;

        public int MaxDist => _maxDist;
        public int MinDist => _minDist;
        public int Score => _score;

        public int Health
        {
            get => _currentHealth;
            set => _currentHealth = value;
        }

        private const float _accel = 0.96f;

        private Weapon _rocketLauncher;
        private Weapon _weapon;

        private Vec2 _position = Vec2.GetZero();
        private Vec2 _velocity = Vec2.GetZero();
        private float _rotateVelo = 0;
        private float _alpha = 0;

        private const float _friction = 0.94f;
        private const int _minDist = 560;
        private const int _maxDist = 1250;
        private const int _maxAngle = 25;

        private Healthbar _healthbar;
        private int _currentHealth = 100;

        private int _score;

        private bool showHealth = true;

        private const int regenrate = 10;

        public Player() : base("assets/character/player.png", 1, 1)
        {
            SetOrigin(width / 2f, height / 2f);
            _weapon = new Weapon(this, Bullet.PLAYER, 5);
            _weapon.Clip = -1;
            _rocketLauncher = new Weapon(this, Bullet.ROCKET, 12);
            _rocketLauncher.Clip = 4000000;
            rotation = 270;
            _healthbar = new Healthbar(this);
            _healthbar.rotation = 270;
            _healthbar.x = -60;
        }

        public void Update()
        {
            _healthbar.Update(100, _currentHealth);
            if (SpaceInvaders.instance.Level == null || SpaceInvaders.instance.Level.Paused || !SpaceInvaders.instance.Level.Planet.Alive()) return;

            if (_currentHealth <= 0)
            {
                Destroy();
                return;
            }

            controlShooting();

            updatePosition();
            updateControls();
            //Console.WriteLine(_velocity.x);
            applyVelocity();
            _velocity *= _friction;
            _rotateVelo *= _friction;

            if (Mathf.Abs(_rotateVelo) < 0.05) _rotateVelo = 0;
            cutVector(_velocity, 0.05f);

            calcRotation();
        }

        private void calcRotation()
        {
            Vec2 offset = (SpaceInvaders.instance.Level.Planet.Position.Clone() - _position);
            rotation = offset.GetAngle(true) + 180;
        }

        private void controlShooting()
        {
            if (Input.GetKeyDown(Settings.P1Fire1))
            {
                _weapon.holdTrigger();
            }
            else if (Input.GetKeyUp(Settings.P1Fire1))
            {
                _weapon.releaseTrigger();
            }

            if (Input.GetKeyDown(Settings.P1Fire2))
            {
                _rocketLauncher.holdTrigger();
            }
            else if (Input.GetKeyUp(Settings.P1Fire2))
            {
                _rocketLauncher.releaseTrigger();
            }

            _rocketLauncher.Update();
            _weapon.Update();
        }

        private void cutVector(Vec2 vector, float value)
        {
            if (Mathf.Abs(vector.x) < value) vector.x = 0;
            if (Mathf.Abs(vector.y) < value) vector.y = 0;
        }

        private void applyVelocity()
        {
            SpaceInvaders game = SpaceInvaders.instance;

            applyHorizontalVelo(game);
            applyVerticalVelo(game);
        }

        private void applyHorizontalVelo(SpaceInvaders game)
        {
            float radius = game.Level.Planet.Position.Clone().Subtract(Position).Length();

            if (Math.Abs(_rotateVelo) > 0)
            {
                float alpha = Vec2.Rad2Deg(Mathf.Sin(_rotateVelo / 2 / (radius / 10)));

                //   Console.WriteLine(angle);

                if ((alpha < 0 && rotation > (270 - _maxAngle)) || (alpha > 0 && rotation < (270 + _maxAngle)))
                {
                    Vec2 newPos = _position.Clone().RotateAround(game.Level.Planet.Position, alpha, true);
                    _position.SetXY(newPos);
                }
                else
                {
                    game.Level.RotateLevel(alpha);
                }
            }
        }

        private void applyVerticalVelo(SpaceInvaders game)
        {
            float radiusNew = game.Level.Planet.Position.Clone().Subtract(Position.Clone().Add(_velocity)).Length();

            if (radiusNew > _minDist && radiusNew < _maxDist) // avoid colision with the planet
            {
                _position += _velocity;
            }
            else
            {
                _velocity.SetXY(0, 0);
            }
        }

        private void updateControls()
        {
            if (Input.GetKey(Settings.P1Up))
            {
                _velocity.Add(new Vec2(rotation, true).Scale(_accel));
            }

            if (Input.GetKey(Settings.P1Down))
            {
                _velocity.Add(new Vec2(rotation, true).Scale(-_accel));
            }

            if (Input.GetKey(Settings.P1Right))
            {
                _rotateVelo += _accel / 6;
            }

            if (Input.GetKey(Settings.P1Left))
            {
                _rotateVelo -= _accel / 6;
            }
        }

        private void updatePosition()
        {
            SetXY(_position.x, _position.y);
        }

        public void recieveDamge(int damage)
        {
            _currentHealth -= damage;
        }

        public bool Alive()
        {
            return _currentHealth > 0;
        }

        public void AddScore(int i)
        {
            _score += i;
        }
    }
}