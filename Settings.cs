﻿using System.Collections.Generic;
using System;
using System.IO;

// Version 1.0, 16-11-2017.

/// <summary>
/// Static class that contains various settings, initialized when the program starts
/// by reading a settings.txt file.
/// 
/// These settings can be retrieved here via get methods (GetInt, GetFloat, GetBool, GetString). 
/// For standard settings (width, height, arcade machine keys), properties are implemented, for faster access.
/// </summary>
public class Settings
{
	public static string SettingsFileName = "config/settings.txt"; // should be in bin/Debug or bin/Release. Use "MySubFolder/settings.txt" for subfolders.
	public static bool Wordy = true;  // If true, settings parsing progress is printed to console
	public static bool ThrowExceptionOnMissingSetting = true; 

	// Standard GXPEngine / arcade machine settings:
	// Use these instead of Game.Width & Game.Height!:
	public static int Width {get {return _width;}}
	public static int Height {get {return _height;}}

	// Use these keys for player controls:
	public static int P1Up {get {return _P1Up;}}
	public static int P1Left { get { return _P1Left; } }
	public static int P1Down { get { return _P1Down; } }
	public static int P1Right { get { return _P1Right; } }
	public static int P1Fire1 { get { return _P1Fire1; } }
	public static int P1Fire2 { get { return _P1Fire2; } }
	public static int P1Fire3 { get { return _P1Fire3; } }
	public static int P1Fire4 { get { return _P1Fire4; } }
	public static int P1Fire5 { get { return _P1Fire5; } }
	public static int P1Fire6 { get { return _P1Fire6; } }

	public static int P2Up { get { return _P2Up; } }
	public static int P2Left { get { return _P2Left; } }
	public static int P2Down { get { return _P2Down; } }
	public static int P2Right { get { return _P2Right; } }
	public static int P2Fire1 { get { return _P2Fire1; } }
	public static int P2Fire2 { get { return _P2Fire2; } }
	public static int P2Fire3 { get { return _P2Fire3; } }
	public static int P2Fire4 { get { return _P2Fire4; } }
	public static int P2Fire5 { get { return _P2Fire5; } }
	public static int P2Fire6 { get { return _P2Fire6; } }

	public static int Start1P { get { return _Start1P; } }
	public static int Start2P { get { return _Start2P; } }
	public static int Menu { get { return _Menu; } }

	public static int GetInt(string key) {
		if (_intSettings.ContainsKey(key))
			return _intSettings[key];
		else {
			Warn("integer setting not found: " + key);
			return 1;
		}
	}

	public static float GetFloat(string key) {
		if (_floatSettings.ContainsKey(key))
			return _floatSettings[key];
		else if (_intSettings.ContainsKey(key)) {
			Warn("float setting replaced by int setting: " + key,true);
			return _intSettings[key];
		}
		else {
			Warn("float setting not found: " + key);
			return 1;
		}
	}

	public static bool GetBool(string key) {
		if (_booleanSettings.ContainsKey(key))
			return _booleanSettings[key];
		else {
			Warn("bool setting not found: " + key);
			return true;
		}
	}

	public static bool HasString(string key) {
		return _stringSettings.ContainsKey(key);
	}

	public static string GetString(string key) {
		if (_stringSettings.ContainsKey(key))
			return _stringSettings[key];
		else {
			Warn("string setting not found: " + key);
			return "setting not found";
		}
	}

	//////////////// PRIVATE FIELDS & METHODS: ///////////////////////

	private static int _width = 800;
	private static int _height = 600;

	private static int _P1Up = 87;	  // W
	private static int _P1Left = 65;  // A
	private static int _P1Down = 68;  // S
	private static int _P1Right = 83; // D
	private static int _P1Fire1 = 70; // F
	private static int _P1Fire2 = 71; // G
	private static int _P1Fire3 = 72; // H
	private static int _P1Fire4 = 86; // V
	private static int _P1Fire5 = 66; // B
	private static int _P1Fire6 = 78; // N

	private static int _P2Up = 73;    // I
	private static int _P2Left = 74;  // J
	private static int _P2Down = 75;  // K
	private static int _P2Right = 76; // L
	private static int _P2Fire1 = 306; // NUMPAD 4
	private static int _P2Fire2 = 307; // NUMPAD 5
	private static int _P2Fire3 = 308; // NUMPAD 6
	private static int _P2Fire4 = 303; // NUMPAD 1
	private static int _P2Fire5 = 304; // NUMPAD 2
	private static int _P2Fire6 = 306; // NUMPAD 3

	private static int _Start1P = 49; // ONE	
	private static int _Start2P = 50; // TWO
	private static int _Menu = 294; // ENTER


	private static float _HudHeight = 2.5f;
	
	private static Dictionary<string, string> _stringSettings;
	private static Dictionary<string, bool> _booleanSettings;
	private static Dictionary<string, float> _floatSettings;
	private static Dictionary<string, int> _intSettings;

	// Force initialization at start of program:
	private static Settings SettingsInstance=new Settings();

	public Settings() {
		Load();
	}

	private static void Warn(string pWarning, bool alwaysContinue = false) {
		string message = "Settings.cs: " + pWarning;
		if (ThrowExceptionOnMissingSetting && !alwaysContinue)
			throw new Exception(message);
		else
			Console.WriteLine("WARNING: "+message);
	}

	private static void Load()
	{
		if (Wordy) Console.WriteLine("Reading settings from file");
		_stringSettings = new Dictionary<string, string>();
		_booleanSettings = new Dictionary<string, bool>();
		_floatSettings = new Dictionary<string, float>();
		_intSettings = new Dictionary<string, int>();

		if (!File.Exists(SettingsFileName)) {
            Warn("No settings file found");
			return;
		}

		StreamReader reader = new StreamReader(SettingsFileName);

		string line = reader.ReadLine();
		while (line != null)
		{
			if (line.Length < 2 || line.Substring(0, 2) != "//")
			{
				if (Wordy) Console.WriteLine("Read a non-comment line: " + line);
				string[] words = line.Split('='); 
				if (words.Length == 2)
				{
					// Remove all white space characters at start and end (but not in between non-white space characters):
					words[0] = words[0].Trim();
					words[1] = words[1].Trim();

					bool boolValue;
					float floatValue;
					int intValue;
					// InvariantCulture is necessary to override (e.g. Dutch) locale settings when using .NET: the decimal separator is a dot, not a comma.
					if (int.TryParse(words[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out intValue))
					{
						_intSettings[words[0]] = intValue;
						SetField(words[0], intValue);
						if (Wordy) Console.WriteLine(" integer argument: Key {0} Value {1}",words[0],intValue);
					}
					else if (float.TryParse(words[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture,
											out floatValue))
					{
						_floatSettings[words[0]] = floatValue;
						if (Wordy) Console.WriteLine(" float argument: Key {0} Value {1}",words[0],floatValue);
					}
					else if (bool.TryParse(words[1], out boolValue))
					{
						_booleanSettings[words[0]] = boolValue;
						if (Wordy) Console.WriteLine(" boolean argument: Key {0} Value {1}",words[0],boolValue);
					}
					else
					{
						_stringSettings[words[0]] = words[1];
						if (Wordy) Console.WriteLine(" string argument: Key {0} Value {1}",words[0],words[1]);
					}
				}
				else
				{
					Warn("Malformed line (expected one '=' character): "+line);
				}
			}
			else
			{
				if (Wordy) Console.WriteLine("Comment line or empty line: " + line);
			}
			line = reader.ReadLine();
		}
		reader.Close();
	}

	private static void SetField(string pName, int pValue) {
		switch (pName) {
			case "Width": _width = pValue; break;
			case "Height": _height = pValue; break;
			case "P1Up": _P1Up = pValue; break;
			case "P1Left": _P1Left = pValue; break;
			case "P1Down": _P1Down = pValue; break;
			case "P1Right": _P1Right = pValue; break;
			case "P1Fire1": _P1Fire1 = pValue; break;
			case "P1Fire2": _P1Fire2 = pValue; break;
			case "P1Fire3": _P1Fire3 = pValue; break;
			case "P1Fire4": _P1Fire4 = pValue; break;
			case "P1Fire5": _P1Fire5 = pValue; break;
			case "P1Fire6": _P1Fire6 = pValue; break;
			case "P2Up": _P2Up = pValue; break;
			case "P2Left": _P2Left = pValue; break;
			case "P2Down": _P2Down = pValue; break;
			case "P2Right": _P2Right = pValue; break;
			case "P2Fire1": _P2Fire1 = pValue; break;
			case "P2Fire2": _P2Fire2 = pValue; break;
			case "P2Fire3": _P2Fire3 = pValue; break;
			case "P2Fire4": _P2Fire4 = pValue; break;
			case "P2Fire5": _P2Fire5 = pValue; break;
			case "P2Fire6": _P2Fire6 = pValue; break;
			case "Start1P": _Start1P = pValue; break;
			case "Start2P": _Start2P = pValue; break;
		}
	}
}
