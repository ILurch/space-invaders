﻿using System;
using System.Collections.Generic;

namespace GXPEngine
{
    public class SpaceInvaders : Game
    {
        public enum GameState
        {
            MAIN,
            GAME,
            END
        }

        public static SpaceInvaders instance;
        public static int FPS = 60;
        public static Settings settings;

        public static void Main(string[] args)
        {
            settings = new Settings();
            instance = new SpaceInvaders(Settings.GetInt("ScreenResolutionX"), Settings.GetInt("ScreenResolutionY"), Settings.GetBool("FullScreen"));
            //instance.scaleX = 1f * Settings.GetInt("ScreenResolutionX") / Settings.Width;
            //instance.scaleY = 1f * Settings.GetInt("ScreenResolutionY") / Settings.Height;
            instance.Start();
        }

        public Level Level => _level;
        public HUD HUD => _hud;

        private HUD _hud;
        private Level _level;
        private FPS _fps;

        private SoundChannel _musicChannel;
        private Sound _currentSound;
        private Sound _lastSound;
        List<Sound> _sounds = new List<Sound>();

        private MainMenu _mainMenu;

        public SpaceInvaders(int pWidth, int pHeight, bool fullscreen) : base(pWidth, pHeight, fullscreen)
        {
            //Console.WriteLine(pWidth + " " + pHeight);
            // SetScaleXY((Settings.Width / 1920f), (Settings.Height / 1080f));

            _fps = new FPS();

            _sounds.Add(new Sound("assets/audio/music.mp3", false, true));
            _sounds.Add(new Sound("assets/audio/music1.mp3", false, true));
            _sounds.Add(new Sound("assets/audio/music2.mp3", false, true));
            _sounds.Add(new Sound("assets/audio/music3.mp3", false, true));

            AddChild(_fps);
            ShowMenu();
            //PlayMusic();
        }

        public void StartGame()
        {
            if (_mainMenu != null)
                _mainMenu.Destroy();

            _level = new Level(this);
            AddChild(_level);

            _hud = new HUD(width, height);
            _hud.SetXY(width / 2f, height / 2f);
            AddChild(_hud);
        }

        public void ShowMenu()
        {
            _mainMenu = new MainMenu(width, height);

            if (_level != null)
            {
                _level.Destroy();
                _level = null;
                _mainMenu.Overlay.visible = false;
            }

            _hud?.Destroy();

            _mainMenu.SetXY(width / 2f, height / 2f);

            AddChild(_mainMenu);
        }

        public void PlayMusic()
        {
            _currentSound = giveRandomMusic();
            _musicChannel = _currentSound.Play();
        }

        public void Update()
        {
            if (Input.GetKeyDown(Key.P))
            {
                Level.Paused = !Level.Paused;
            }

            if (_level != null && Input.GetKeyDown(Settings.Menu))
            {
                ShowMenu();
            }

            if (_musicChannel != null && !_musicChannel.IsPlaying)
            {
                _lastSound = _currentSound;
                _currentSound = giveRandomMusic();
                _musicChannel = _currentSound.Play();
            }

            SetChildIndex(_fps, 10000);
        }

        private Sound giveRandomMusic()
        {
            Sound sound = null;
            do
            {
                sound = _sounds[Utils.Random(0, _sounds.Count)];
            } while (sound.Equals(_lastSound));

            return sound;
        }
    }
}