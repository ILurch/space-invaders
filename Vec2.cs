﻿using System;
using System.Runtime.InteropServices;
using GXPEngine.Core;
using Vec2 = GXPEngine.Vec2;

namespace GXPEngine
{
    public class Vec2
    {
        public enum Direction
        {
            LEFT,
            RIGHT,
            UP,
            DOWN
        }

        public float x;
        public float y;

        public Vec2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public Vec2(Vec2 vec)
        {
            x = vec.x;
            y = vec.y;
        }

        public Vec2(float angle, bool degree = false)
        {
            SetAngle(angle, degree);
        }

        public float Length()
        {
            return (float) Math.Sqrt(x * x + y * y);
        }

        public float RawLength()
        {
            return (x * x + y * y);
        }

        public Vec2 Normalize()
        {
            float length = Length();
            if (length <= float.Epsilon)
                return this;
            x /= length;
            y /= length;
            return this;
        }

        public Vec2 Add(float x, float y)
        {
            return Add(new Vec2(x, y));
        }

        public Vec2 Subtract(float x, float y)
        {
            return Subtract(new Vec2(x, y));
        }

        public Vec2 Add(Vec2 other)
        {
            x += other.x;
            y += other.y;
            return this;
        }

        public Vec2 Subtract(Vec2 other)
        {
            x -= other.x;
            y -= other.y;
            return this;
        }

        public float DistanceSquared(Vec2 other)
        {
            return new Vec2(x - other.x, y - other.y).Length();
        }

        public float DistanceNotSquared(Vec2 other)
        {
            return new Vec2(x - other.x, y - other.y).RawLength();
        }

        public Vec2 Scale(float scalar)
        {
            x *= scalar;
            y *= scalar;
            return this;
        }

        public Vec2 SetXY(float x, float y)
        {
            this.x = x;
            this.y = y;
            return this;
        }

        public Vec2 SetXY(Vec2 vec2)
        {
            x = vec2.x;
            y = vec2.y;
            return this;
        }

        public float GetAngle(bool degrees = false)
        {
            float angle = Mathf.Atan2(y, x);
            if (degrees)
                angle = Rad2Deg(angle);
            return angle;
        }

        public Vec2 SetAngle(float angle, bool degrees = false)
        {
            float angleR = degrees ? Deg2Rad(angle) : angle;

            float length = Length();
            if (length == 0) length = 1;

            x = Mathf.Cos(angleR);
            y = Mathf.Sin(angleR);

            return Scale(length);
        }

        public Vec2 Rotate(float angle, bool degrees = false)
        {
            float angleR = degrees ? Deg2Rad(angle) : angle;

            float angleCos = Mathf.Cos(angleR);
            float angleSin = Mathf.Sin(angleR);

            Vec2 temp = Clone();
            
            x = temp.x * angleCos - temp.y * angleSin;
            y = temp.x * angleSin + temp.y * angleCos;
            return this;
        }

        public Vec2 RotateAround(Vec2 position, float angle, bool degrees = false)
        {
            
            Subtract(position).Rotate(angle, degrees);
            Add(position);
            return this;
        }

        public Vec2 SetRotation(Vec2 center, float angle, bool degrees = true)
        {
            Subtract(center).SetAngle(angle, degrees);
            Add(center);
            return this;
        }

        public Vec2 Normal()
        {
            return new Vec2(-1 * y, x).Normalize();
        }

        public Vec2 Reflect(Vec2 normal, float bounciness)
        {
            float dot = this.Dot(normal);
            float c = 1 + bounciness;
            Vec2 vOut = this - normal.Scale(c * dot);
            return vOut;
        }

        public float Dot(Vec2 other)
        {
            return x * other.x + y * other.y;
        }

        public Vec2 Clone()
        {
            return new Vec2(this);
        }

        public static Vec2 operator +(Vec2 v1, Vec2 v2)
        {
            v1.Add(v2);
            return v1.Clone();
        }

        public static Vec2 operator -(Vec2 v1, Vec2 v2)
        {
            v1.Subtract(v2);
            return v1.Clone();
        }

        public static Vec2 operator *(Vec2 v, float a)
        {
            v.x *= a;
            v.y *= a;
            return v.Clone();
        }

        public static Vec2 operator /(Vec2 v, float a)
        {
            v.x /= a;
            v.y /= a;
            return v.Clone();
        }

        public static float Deg2Rad(float angleDegree)
        {
            return Mathf.PI * angleDegree / 180.0f;
        }

        public static float Rad2Deg(float angleRadian)
        {
            return angleRadian * (180.0f / Mathf.PI);
        }

        public static Vec2 GetUnitVector(float angle, bool degrees = false)
        {
            return new Vec2(angle, degrees).Normalize();
        }

        /*    public static Vec2 GetRandomUnitVector()
            {
                float angleR = Utils.Random(-Mathf.PI, Mathf.PI);
                return GetUnitVector(angleR);
            }
        */
        public override string ToString()
        {
            return $"Vec2: ({x}, {y})";
        }

        public static Vec2 GetZero()
        {
            return new Vec2(0, 0);
        }

        public static Vec2 ByDirection(Direction dir)
        {
            switch (dir)
            {
                case Direction.LEFT:
                    return new Vec2(-1, 0);
                case Direction.RIGHT:
                    return new Vec2(1, 0);
                case Direction.UP:
                    return new Vec2(0, 1);
                case Direction.DOWN:
                    return new Vec2(0, -1);
                default:
                    return GetZero();
            }
        }
    }
}