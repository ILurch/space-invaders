﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using GXPEngine.Parse;

namespace GXPEngine
{
    public class WaveManager
    {
        public Wave CurrentWave => _currentWave >= 0 && _currentWave < _waves.waves.Length ? _waves.waves[_currentWave] : null;
        public int Level => level;

        private int level;

        private int _lastWave = -1;
        private int _currentWave = 0;
        private int _currentEnemy = 0;

        private int count;
        private int delayCount;

        private int totalWaves;

        private Waves _waves;
        private bool nextWave = true;

        private List<Enemy> _waveEnemies = new List<Enemy>();

        public WaveManager()
        {
            _waves = WaveParser.ParseWaveFile("waves.xml");
        }

        private bool waveCleared()
        {
            if (CurrentWave != null && _waveEnemies.Count == CurrentWave.Enemies.Length)
            {
                foreach (Enemy enemy in _waveEnemies)
                {
                    if (enemy.Alive())
                        return false;
                }

                return true;
            }
            return false;
        }

        public void Update()
        {
            if (SpaceInvaders.instance.Level == null || SpaceInvaders.instance.Level.Paused) return;
            if (_currentWave > _waves.waves.Length - 1)
            {
                _currentWave = 0;
                level++;
            }

            // fist wave without delay
            if (totalWaves == 0)
            {
                delayCount = _waves.delay + 1;
            }

            if (delayCount > _waves.delay)
            {
                if (waveCleared())
                {
                    _currentWave++;
                    _currentEnemy = 0;
                    delayCount = 0;
                    nextWave = true;
                }

                if (CurrentWave == null || _currentEnemy == CurrentWave.Enemies.Length) return;
                WaveEnemy currentEnemy = CurrentWave.Enemies[_currentEnemy];

                if (nextWave)
                {
                    totalWaves++;
                    Console.WriteLine("total waves=" + totalWaves);
                    nextWave = false;
                    SpaceInvaders.instance.HUD.SetWaveCount(totalWaves);
                    _waveEnemies.Clear();
                }

                if (count >= currentEnemy.delay)
                {
                    Console.WriteLine("Spawning Enemy " + (_currentEnemy + 1) + "/" + CurrentWave.Enemies.Length);
                    Level level = SpaceInvaders.instance.Level;

                    Vec2 posOffset = new Vec2(currentEnemy.rotation, true).Scale(1600);
                    Enemy enemy = new Enemy(getType(currentEnemy.type), level.Planet.Position.Clone().Add(posOffset));
                    level.Enemies.Add(enemy);
                    level.AddChild(enemy);
                    _currentEnemy++;
                    _waveEnemies.Add(enemy);
                    //Console.WriteLine((_currentWave + 1) + ". Wave: Enemy (" + _currentEnemy + "/" + CurrentWave.Enemies.Length + ") spawned");
                    count = 0;
                }

                count++;
            }

            _lastWave = _currentWave;
            delayCount++;
        }

        private EnemyType getType(string name)
        {
            switch (name)
            {
                case("default"):
                    return Enemy.DEFAULT;
                case("shooter"):
                    return Enemy.SHOOTER;
                case("kamikaze"):
                    return Enemy.KAMIKAZE;
                default:
                    return null;
            }
        }
    }
}