﻿using System;
using System.Runtime.InteropServices;

namespace GXPEngine
{
    public class Weapon
    {
        public bool Shooting => _shooting;

        public GameObject Owner => _owner;

        public int Clip
        {
            get => _currentClip;
            set => _currentClip = value;
        }

        private GameObject _owner;
        private bool _shooting;

        private float startSpeed;
        private float speed;

        private int _currentClip;
        private int _count;

        private BulletType _type;

        private const float _slowlyness = 1.05f;

        public Weapon(GameObject owner, BulletType type, int speed)
        {
            _owner = owner;
            _type = type;
            this.speed = speed;
            startSpeed = speed;
        }

        public void holdTrigger()
        {
            _shooting = true;
        }

        public void releaseTrigger()
        {
            _shooting = false;
        }

        public void Shoot()
        {
            if (_owner != null && _owner.parent != null)
            {
                Bullet bullet = new Bullet(_type, _owner.rotation);
                bullet.Update();
                bullet.Position.SetXY(_owner.x, _owner.y);
                _owner.parent.AddChild(bullet);
                SpaceInvaders.instance.Level.Bullets.Add(bullet);

                Sound sound = new Sound("assets/audio/" + bullet.Type.Sound);
                sound.Play();

                _count = 0;
                _currentClip--;
                speed *= _slowlyness;
                if (speed > 20) speed = 20;
            }
        }

        public void Update()
        {
            if (_count > speed && (_currentClip > 0 || _currentClip < 0))
            {
                if (_shooting)
                    Shoot();
                else
                {
                    speed -= 0.4f;
                    if (speed < startSpeed)
                        speed = startSpeed;
                }
            }
            else
                _count++;
        }
    }
}